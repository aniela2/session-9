package ro.aniela;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Exercise3 {

    public static void main(String[] args) {
        City ro = new Metropolis(12345);
        City an = new Village(123);
        City m = new Metropolis(123456);
        City k = new SmallTown(1);

        List<City> cities = List.of(ro, an, m, k);
        System.out.println(categorizeTowns(cities));
    }

    private static Map<Class<?>, List<City>> categorizeTowns(List<City> cities) {
        Map<Class<?>, List<City>> map = new HashMap<>();
        for (City c : cities) {
            if (map.containsKey(c.getClass())) {
                map.get(c.getClass()).add(c);
            } else {
                List<City> list = new ArrayList<>();
                list.add(c);
                map.put(c.getClass(), list);
            }
        }
        return map;
    }


}
