package ro.aniela;

public class Author {
    private String gender;
    private String name;
    private String surname;

    public Author(String gender, String name, String surname) {
        this.gender = gender;
        this.name = name;
        this.surname = surname;
    }

    public String getGender() {
        return gender;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }
}
