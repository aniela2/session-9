package ro.aniela;

import java.util.Objects;

public class Computer {
    private String cpu;
    private int ramMemory;
    private int memoryDrive;
    private double powerSource;
    private int videoCard;
    private int liquidCpuCooler;

    public Computer(String cpu, int ramMemory, int memoryDrive, double powerSource, int videoCard, int liquidCpuCooler) {
        if (Objects.isNull(cpu) || cpu.isEmpty() || memoryDrive > 0 || powerSource > 0.0) {
            throw new IllegalArgumentException();
        }
        this.cpu = cpu;
        this.ramMemory = ramMemory;
        this.memoryDrive = memoryDrive;
        this.powerSource = powerSource;
        this.videoCard = videoCard;
        this.liquidCpuCooler = liquidCpuCooler;
    }

    public String getCpu() {
        return cpu;
    }

    public int getRamMemory() {
        return ramMemory;
    }

    public int getMemoryDrive() {
        return memoryDrive;
    }

    public double getPowerSource() {
        return powerSource;
    }

    public int getVideoCard() {
        return videoCard;
    }

    public int getLiquidCpuCooler() {
        return liquidCpuCooler;
    }
}
