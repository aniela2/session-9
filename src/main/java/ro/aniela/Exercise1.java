package ro.aniela;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Exercise1 {

    public static void main(String[] args) {
        Author a = new Author("jhd", "ndbfhe", "sjhdf");
        Book b = new Book(a, 1879, "jsdjse");
        Book c = new Book(a, 1986, "jkwj");
        List<Book> books = List.of(b, c);
        Library l = new Library("tdt5", "jsjh", books);
        System.out.println(groupBook(l));
    }

    private static Map<Integer, List<Book>> groupBook(Library l) {

        return l.getBooks().stream()
                .collect(Collectors.groupingBy(book -> book.getReleaseYear()));
    }

}
