package ro.aniela;

import java.util.ArrayList;
import java.util.List;

public class Exercise4 {
    public static void main(String[] args) {
        System.out.println(uniqueCharacterOrNot("123"));
    }

    private static boolean uniqueCharacterOrNot(String given) {
        String[] args = given.split("");
        List<String> b = new ArrayList<>();
        for (String s : args) {
            if (!(b.contains(s))) {
                b.add(s);
            } else {
                return false;
            }
        }
        return true;
    }
}