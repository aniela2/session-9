package ro.aniela;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class exercise2 {
    public static void main(String[] args) {
        Integer[] list = {1, 2, 3, 4};
        System.out.println();
        Pair p = theHalf(list);
        System.out.println(Arrays.toString((Integer[]) p.getT()));
        System.out.println(Arrays.toString((Integer[]) p.getV()));
    }

    private static Pair<Integer[], Integer[]> theHalf(Integer[] numbers) {

        int lA = numbers.length / 2;
        Integer[] firstHalf = new Integer[lA];
        Integer[] secondHalf = new Integer[lA];
        int j = 0;
        for (int i = 0; i < numbers.length; i++) {
            if (i < lA) {
                firstHalf[i] = numbers[i];
            } else {
                secondHalf[j] = numbers[i];
                j++;
            }
        }
        return new Pair<>(firstHalf, secondHalf);
    }
}

