package ro.aniela;

import java.util.List;

public class City {
    private int population;
    public City(int population){
        this.population=population;
    }

    public int getPopulation() {
        return population;
    }
}
