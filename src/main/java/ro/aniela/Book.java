package ro.aniela;

import java.net.Authenticator;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class Book {
    private Author author;
    private int releaseYear;
    private String name;

    public Book(Author author, int releaseYear, String name) {
        this.author = author;
        this.releaseYear = releaseYear;
        this.name = name;
    }

    public Author getAuthor() {
        return author;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public String getName() {
        return name;
    }

}
