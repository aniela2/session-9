package ro.aniela;

import java.util.List;

public class Library {
    private String address;
    private String name;
    private List<Book> books;

    public Library(String address, String name, List<Book> books) {
        this.address = address;
        this.name = name;
        this.books = books;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public List<Book> getBooks() {
        return books;
    }
}
