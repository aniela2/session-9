package ro.aniela;

public class Metropolis extends City {
    public Metropolis(int population) {
        super(population);
    }

    @Override
    public String toString() {
        return "Metropolis has Population" + getPopulation();
    }
}
